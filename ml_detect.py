from collections import deque
import logging
import os

from ediblepickle import checkpoint
import numpy as np
import pandas as pd
from tqdm import tqdm


@checkpoint(
    key=lambda args, kwargs: 'ml_detect__load_transactions__{}.cache'.format(
        args[0].replace('/', '__').replace('.', '_'),
    ),
)
def load_transactions(fpath):
    """Load transactions data from CSV file."""
    data = pd.read_csv(fpath, sep='|')

    # "2006-02-29" is not a legitimate date. Assign observations to
    # 2006-02-28, which seems like a reasonable adjustment for this
    # data set since the that date has only 701 records compared to
    # 17,512 records for "2006-02-29."
    data.replace({'TIMESTAMP': {'2006-02-29': '2006-02-28'}}, inplace=True)

    # Streamline column names
    data.rename(columns={'TRANSACTION': 'XACTID'}, inplace=True)
    data.rename(str.lower, axis='columns', inplace=True)

    # Sort the records by timestamp since financial transaction data
    # is naturally generated in chronological order. This step is not
    # necessary, but it expedites data examination.
    data.sort_values('timestamp', inplace=True)

    return data


def detect_money_laundry_xacts(x, max_fee=10, min_fee=0):
    """Detect potential money laundering transactions.

    Detect potential money laundering transactions in DataFrame `x`.
    Return a set of suspicious transaction IDs.

    """
    assert max_fee >= min_fee

    # Filter records with distinct sender and receiver
    x = x[x['sender'] != x['receiver']]

    # Identify potential bridgers
    i = x['receiver'].isin(x['sender'])
    bridge_space = x['receiver'][i].unique()

    # Aggregate amounts by sender-receiver pairs
    x_bridged = (
        x[
            (x['sender'].isin(bridge_space))
            | (x['receiver'].isin(bridge_space))
        ]
        .groupby(['sender', 'receiver'])
        .agg({'amount': np.sum, 'xactid': lambda x: tuple(x.tolist())})
    )

    # Identify potential money laundering transactions
    mlxacts = set()
    for ent in bridge_space:
        ent_recv = x_bridged.loc[(slice(None), ent), :]
        ent_send = x_bridged.loc[(ent, slice(None)), :]
        for qr, xidr in ent_recv.itertuples(index=False):
            for qs, xids in ent_send.itertuples(index=False):
                if (
                    qs >= (1 - 0.01 * max_fee) * qr
                    and qs <= (1 - 0.01 - min_fee) * qr
                ):
                    mlxacts.update(xidr, xids)
    return mlxacts


class MoneyLaundryIterator():
    """Date-based iterator for identifying money laundering transactions.

    We use this class to iterate over the windows of the transaction
    data, returning potentially multi-day windows on each iteration.

    The window skips dates that are not found in the data. For
    example, the transaction data does not include transactions taking
    place on 2005-12-05, so a window of 2 includes transactions from
    2005-12-04 and 2005-12-06.

    """

    def __init__(self, data, window=1, start=None, end=None):
        self.data = data
        self.window = window    # Number of days with transaction data
        self.start = start
        self.end = end

        self.date_domain = np.sort(self.data['timestamp'].unique())
        if start is not None:
            self.date_domain = self.date_domain[self.date_domain >= start]
        if end is not None:
            self.date_domain = self.date_domain[self.date_domain < end]
        self.date_domain_itr = np.nditer(self.date_domain, flags=['refs_ok'])
        self.xact_queue = deque(maxlen=self.window)

    def __iter__(self):
        return self

    def __next__(self):
        self.xact_queue.append(self._get_transactions_for_next_date())
        while len(self.xact_queue) < self.xact_queue.maxlen:
            self.xact_queue.append(self._get_transactions_for_next_date())

        return pd.concat(self.xact_queue)

    def _get_transactions_for_next_date(self):
        date = next(self.date_domain_itr).item()
        return self.data[self.data['timestamp'] == date]

    def nwindows(self):
        """Return the number of windows to iterate over."""
        return len(self.date_domain) - self.window + 1


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)

    # Load transaction data
    logging.info('Loading data...')
    fpath_csv = os.path.join('data', 'transactions.zip')
    data = load_transactions(fpath_csv)

    # Detect potential money laundering transactions
    logging.info('Detecting suspicious transactions...')
    mlxacts = set()
    mlitr = MoneyLaundryIterator(data, window=1)
    for x in tqdm(mlitr, total=mlitr.nwindows()):
        xids = detect_money_laundry_xacts(x, max_fee=10)
        mlxacts.update(xids)

    # Isolate suspicious transactions and entities
    logging.info('Isolating suspicious transactions and entities...')
    ml_data = data[data['xactid'].isin(mlxacts)]
    ml_entities = (
        pd.concat([
            ml_data[['sender']].rename(columns={'sender': 'entity'}),
            ml_data[['receiver']].rename(columns={'receiver': 'entity'}),
        ])
        .groupby('entity')
        .agg({'entity': np.size})
        .sort_values('entity', ascending=False)
    )

    # Write to file
    logging.info('Exporting suspicious transactions and entities...')
    ml_data[['xactid']].to_csv(
        'suspicious_transactions.csv',
        header=False,
        index=False,
    )
    ml_entities.to_csv(
        'suspicious_entities.csv',
        columns=[],
        header=False,
    )
